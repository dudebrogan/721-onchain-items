// SPDX-License-Identifier: MIT
pragma solidity ^0.8.14;

// import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/governance/TimelockControllerUpgradeable.sol";
import "./GameGovernor.sol";
import "./GameItems.sol";

abstract contract GameBase is Initializable, OwnableUpgradeable, UUPSUpgradeable {
    struct Meta {
        string name;
        string dataHash;
    }

    address public gameGovernor; // can be a game governor contract or a wallet!!
    address public gameItems;
    string[] public metas;
    mapping(string => Meta) metaData;
    uint256[49] __gap;

    function compareStrings(string memory a, string memory b) internal pure returns (bool) {
        return (keccak256(abi.encodePacked((a))) == keccak256(abi.encodePacked((b))));
    }
}

contract Game is GameBase {
    function updateMeta(string calldata _meta) public {
        require(msg.sender == address(gameGovernor), 'Only the governor can change the meta after a successful vote');
        for (uint i=0; i < metas.length; i++) {
            if (compareStrings(_meta, metas[i])) {
                require(false, 'already used this meta name');
            }
        }

        metas.push(_meta);
    }

    function getLatestMeta() external view returns (Meta memory) {
        require(metas.length > 0, 'no metas yet');
        return metaData[metas[metas.length - 1]];
    }

    function getMetaByName(string calldata _meta) external view returns(Meta memory) {
        for (uint i=0; i < metas.length; i++) {
            if (compareStrings(_meta, metas[i])) {
                return metaData[_meta];
            }
        }

        require(false, 'didnt find that meta');
    }

    function setGovernorAddress(address _governor) public onlyOwner {
        gameGovernor = _governor;
    }

    function setGameItemsAddress(address _gameItems) public onlyOwner {
        gameItems = _gameItems;
    }

    function initialize() public initializer {
        __Ownable_init();
        __UUPSUpgradeable_init();      
    }

    ///@dev required by the OZ UUPS module
    function _authorizeUpgrade(address) internal override onlyOwner {}

    ///@dev below required for upgradeable contract
    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }
}