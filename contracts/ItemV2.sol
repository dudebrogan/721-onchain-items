// SPDX-License-Identifier: MIT
pragma solidity ^0.8.14;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./Item.sol";

abstract contract ItemBaseV2 is Item  {
    address public lentTo; // if the address is the 0x00 address, then it is unlent
}

contract ItemV2 is ItemBaseV2 {
    modifier onlyItemOwner() {
        require(msg.sender == itemOwner, "only the item owner can do that");
        _;
    }

    ///@param lendee wallet borrowing item
    function lendTo(address lendee) public onlyItemOwner {
        lentTo = lendee;
    }

    function rescindLend() public onlyItemOwner {
        lentTo = address(0);
    }

    ///@param name stat name
    ///@param value amount to increase by
    function upgradeStat(string calldata name, uint value) public onlyOwner {
        bool found = false;
        for (uint8 i = 0; i < 255; i++) {
            if (keccak256(bytes(stats[i].name)) == keccak256(bytes(name))) {
                found = true;
                stats[i].value += value;
                i = 254;
            }
        }

        require(found, 'Did not find stat to upgrade');
    }

    function downgradeStat(string calldata name, uint value) public onlyOwner {
        bool found = false;
        for (uint8 i = 0; i < 255; i++) {
            if (keccak256(bytes(stats[i].name)) == keccak256(bytes(name))) {
                require(value <= stats[i].value, 'downgrading by too much');
                found = true;
                stats[i].value -= value;
                i = 254;
            }
        }

        require(found, 'Did not find stat to downgrade');
    }

    function updateStat(string calldata name, uint value) public onlyOwner {
        bool added;
        for (uint8 i = 0; i < 255; i++) {
            if (keccak256(bytes(stats[i].name)) == keccak256(bytes(''))) {
                // require(value <= stats[i].value, 'downgrading by too much');
                added = true;
                stats[i].name = name;
                stats[i].value = value;
                i = 254;
            }
        }
    }

    ///@param initStats initial stats for item
    function initialize(address _itemOwner, Stat[] memory initStats) public override initializer {
        itemOwner = _itemOwner;
        for(uint8 i = 0; i < initStats.length; i++) {
            Stat memory stat = initStats[i];
            stats[i].name =  stat.name;
            stats[i].value =  stat.value;
        }

        ///@dev as there is no constructor, we need to initialise the OwnableUpgradeable explicitly
        __Ownable_init();
    }

    ///@dev required by the OZ UUPS module
    function _authorizeUpgrade(address) internal override onlyOwner {}

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }
}