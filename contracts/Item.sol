// SPDX-License-Identifier: MIT
pragma solidity ^0.8.14;

// Open Zeppelin libraries for controlling upgradability and access.
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

abstract contract ItemBase is Initializable, UUPSUpgradeable, OwnableUpgradeable {
    struct Stat {
        string name;
        uint value; // string is possible, but having an int here can make it so on-chain actions can upgrade items
    }

    mapping(uint8 => Stat) public stats;
    address public itemOwner; // The erc-721 holding contract owns this contract
}

contract Item is ItemBase {
    ///@dev no constructor in upgradable contracts. Instead we have initializers
    ///@param initStats initial stats for item
    function initialize(address _itemOwner, Stat[] memory initStats) public virtual initializer {
        itemOwner = _itemOwner;
        for(uint8 i = 0; i < initStats.length; i++) {
            Stat memory stat = initStats[i];
            stats[i].name =  stat.name;
            stats[i].value =  stat.value;
        }

        ///@dev as there is no constructor, we need to initialise the OwnableUpgradeable explicitly
        __Ownable_init();
    }

    ///@dev required by the OZ UUPS module
    function _authorizeUpgrade(address) internal override virtual onlyOwner {}

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }
}