// SPDX-License-Identifier: MIT
pragma solidity ^0.8.14;

import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721VotesUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "./ItemV2.sol";
import "./Game.sol";

abstract contract GameItemsBase is Initializable, ERC721VotesUpgradeable, OwnableUpgradeable, UUPSUpgradeable {
    CountersUpgradeable.Counter internal _tokenIdCounter;
    
    mapping(uint => ItemV2) public tokenToItem;
    Game public game;
}

contract GameItems is GameItemsBase {
    using CountersUpgradeable for CountersUpgradeable.Counter; // usings are not inherited, must declare in each :( 

    function safeMint(address to, ItemV2 item) public onlyOwner {
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        tokenToItem[tokenId] = item;
        _safeMint(to, tokenId);
    }

    function initialize(Game _game) public initializer {
        __ERC721_init("GameItems", "GI");
        __Ownable_init();
        __UUPSUpgradeable_init();
        game = _game;
    }

    ///@dev required by the OZ UUPS module
    function _authorizeUpgrade(address) internal override onlyOwner {}

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }
}