const { ethers } = require('ethers')
const { SignerWithAddress } = require("@nomiclabs/hardhat-ethers/dist/src/signer-with-address")

const getExpectedContractAddress = async (deployer) => {
  const adminAddressTransactionCount = await deployer.getTransactionCount()
  const expectedContractAddress = ethers.utils.getContractAddress({
    from: deployer.address,
    nonce: adminAddressTransactionCount + 2,
  })

  return expectedContractAddress
}

module.exports = {
    getExpectedContractAddress
}