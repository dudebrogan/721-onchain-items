const { ethers, upgrades } = require("hardhat")
const { getExpectedContractAddress } = require('./util')

async function main() {
  const FEE_DATA = {
    maxFeePerGas:         ethers.utils.parseUnits('100', 'gwei'),
    maxPriorityFeePerGas: ethers.utils.parseUnits('5',   'gwei'),
  };


  // Wrap the provider so we can override fee data.
  const provider = new ethers.providers.FallbackProvider([ethers.provider], 1);
  provider.getFeeData = async () => FEE_DATA;
  // const signer = await ethers.getSigner(deployer)

  const Gov = await ethers.getContractFactory("GameGovernor")
  const Game = await ethers.getContractFactory("Game")
  const Timelock = await ethers.getContractFactory("TimelockControllerUpgradeable")
  const GameItems = await ethers.getContractFactory("GameItems")
  const ItemV2 = await ethers.getContractFactory("ItemV2")

  console.log("Deploying game...")

  const game = await upgrades.deployProxy(Game, [], {
    initializer: "initialize",
  })

  await game.deployed()

  console.log("Game deployed to ", game.address)

  const signerAddress = await GameItems.signer.getAddress()
  const gameSigner = await ethers.getSigner(signerAddress)

  console.log("Deploying game items...")

  const gameItems = await upgrades.deployProxy(GameItems, [game.address], {
    initializer: "initialize",
  })

  await gameItems.deployed()

  console.log("GameItems deployed to: ", gameItems.address)

  const item1 = await upgrades.deployProxy(ItemV2, ['0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266', [{ name: 'AOE_Range', value: '2' }]])
  const item2 = await upgrades.deployProxy(ItemV2, ['0x70997970C51812dc3A010C7d01b50e0d17dc79C8', [{ name: 'AOE_Range', value: '2' }]])
  const item3 = await upgrades.deployProxy(ItemV2, ['0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC', [{ name: 'AOE_Range', value: '2' }]])
  const item4 = await upgrades.deployProxy(ItemV2, ['0x90F79bf6EB2c4f870365E785982E1f101E93b906', [{ name: 'AOE_Range', value: '2' }]])

  console.log('items created', item1.address, item2.address, item3.address)

  await gameItems.safeMint('0xf25a32328A596D0B5ACa07aE770E836Bec9DaA1D', item1.address)
  await gameItems.safeMint('0xA55E276d91C654c622352DFD5e4f58dEB43A162E', item2.address)
  await gameItems.safeMint('0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC', item3.address)
  
  console.log('items minted')

  console.log("Deploying timelock...")
  const timelock = await Timelock.deploy()

  await timelock.deployed()

  console.log("timelock deployed to: ", timelock.address)
  console.log("Deploying governor...")

  const gov = await upgrades.deployProxy(Gov, [gameItems.address, timelock.address, game.address], {
    initializer: "initialize",
  })

  await gov.deployed()

  const govOwner =  await gov.owner()
  
  console.log("Governor deployed to:", gov.address)
  console.log("gov owner", govOwner)

  game.setGovernorAddress(gov.address)
  game.setGameItemsAddress(gameItems.address)
}

main()