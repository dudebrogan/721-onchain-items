const { ethers, upgrades } = require("hardhat")

async function main() {
  const [deployer, tokenOwner, lendee] = await ethers.getSigners()
  const Item = await ethers.getContractFactory("GameGovernor")

  console.log("Deploying... ")

  const item = await upgrades.deployProxy(Item, [await tokenOwner.getAddress(), INIT_STATS], {
    initializer: "initialize",
  })

  await item.deployed()
  const initStats = await item.stats(0)
  const owner = await item.itemOwner()

  console.log("Item deployed to:", item.address)
  console.log("first item name: ", initStats.name)
  console.log("first item value: ", Number(initStats.value))
  console.log("first item owner: ", owner)
  console.log("first item tokenOwner: ", tokenOwner.getAddress())

  const upgradedItem = await upgrades.upgradeProxy(item.address, ItemV2)
  
  // await upgradedItem.deployed();
  
  await upgradedItem.connect(tokenOwner).lendTo(lendee.getAddress())
  const is = await upgradedItem.connect(deployer).upgradeStat('Durability', 1)

  await is.wait()

  const newStats = await upgradedItem.stats(0)
  const lendeeAddress = await upgradedItem.lentTo()

  console.log("Lendee Address", lendeeAddress)
  console.log("Item with upgraded utility deployed to:", upgradedItem.address)
  console.log("stat is still: ", newStats.name)
  console.log("upgraded stat value: ", Number(newStats.value))
}

main()