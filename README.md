# Blockchain game with Player Governance Project

This project creates a `Game` contract, which links to a `GameItems` ERC-721 token and a `Governor` that allows NFT holders to vote on the game's meta. They are all upgradeable, but only Item and Game would benefit much from it. You can test the governor below, see the `Test Governance on Tally` section.

Each of the NFTs links to an `ItemV2`, which has been upgrade from `Item`. You can view the governance on tally [here](https://www.tally.xyz/governance/eip155:5:0xa4A2E82563a11A6243D722a283019AD3968C4f97). 

Check out the `deployGame` script to see how it all works. 

## How to test (Only covers Item and upgrade to ItemV2)

`npm install`

Create a `.env` and add values for `DEPLOYER_PRIVATE_KEY, ALCHEMY_API_KEY, and ETHERSCAN_API_KEY` (for test, only the first is needed)

`npx hardhat test`

`npx hardhat coverage  `

`npx hardhat run .\scripts\deploy.js --network localhost`

## Test Governance on Tally
1. go to the deployed governance at [tally](https://www.tally.xyz/governance/eip155:5:0xa4A2E82563a11A6243D722a283019AD3968C4f97). Make sure everything on tally is done on the goerli network. 
2. Import the following two keez (they are dev only accounts, only goerli eth) with no space in the middle on metamask: `15ed7c84e176b8d2e5b2da431eb561 84652343c138585839ee7db7e2d48334ad` and `7600e1531f89d209a9e3afe4eea59 33e68bc1d2d969f06b4072852e3014785bd`
3. You can create a proposal and vote on it there with those accounts.
4. On execution of a successful proposal, the `Game` meta is automatically updated. Cool!

## Project Features

This project had the following test coverage before adding governance:

File         |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
-------------|----------|----------|----------|----------|----------------|
 contracts\  |    85.37 |    71.43 |    83.33 |    85.71 |                |
  Item.sol   |      100 |      100 |      100 |      100 |                |
  ItemV2.sol |    82.35 |    71.43 |    77.78 |    82.86 |... 37,38,39,43 |
-------------|----------|----------|----------|----------|----------------|
All files    |    85.37 |    71.43 |    83.33 |    85.71 |                |
-------------|----------|----------|----------|----------|----------------|

These tests run on a pre commit hook, along with a linter.  

## Functionality

The upgraded contracts allows an item to be updated by adding, upgrading, or downgrading stats (only callable by the contract creator (the `Game`), not who owns the token that points to this). Item owners can also lend their items out to addresses.
