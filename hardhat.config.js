require("@nomiclabs/hardhat-ethers");
require("@openzeppelin/hardhat-upgrades");
require("@nomiclabs/hardhat-etherscan");
require('hardhat-docgen');
require('@nomicfoundation/hardhat-toolbox')

require("dotenv").config();

const { DEPLOYER_PRIVATE_KEY, ALCHEMY_API_KEY, ETHERSCAN_API_KEY } = process.env

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: {
    version: "0.8.17",
    settings: { 
      optimizer: { 
        enabled: true, 
        runs: 200 
      } 
    }
  },
  networks: {
    goerli: {
      url: `https://eth-goerli.g.alchemy.com/v2/${ALCHEMY_API_KEY}`,
      accounts: [`${DEPLOYER_PRIVATE_KEY}`],
      gasPrice: 18000000000,
    }
  },
  docgen: {
    path: './docs',
    clear: true,
    runOnCompile: true
  },
  etherscan: {
    apiKey: ETHERSCAN_API_KEY,
  }
};