const { expect } = require("chai");
const { ethers, upgrades } = require("hardhat")


const INIT_STATS = [
  [
    "Durability",
    2
  ]
]

const zeroAddress = '0x0000000000000000000000000000000000000000'

describe("Item", async() => {
  const reset = async() => {

  }
  describe("Deployment", async() => {
    it('should deploy with correct params', async() => {
      const [deployer, tokenOwner, lendee] = await ethers.getSigners()
      const Item = await ethers.getContractFactory("Item")
      const ItemV2 = await ethers.getContractFactory("ItemV2")

      const item = await upgrades.deployProxy(Item, [await tokenOwner.getAddress(), INIT_STATS], {
        initializer: "initialize",
      })

      await item.deployed()
      const initStats = await item.stats(0)
      const owner = await item.itemOwner()

      expect(initStats.name).to.be.equal('Durability')
      expect(Number(initStats.value)).to.be.equal(2)
      expect(owner).to.be.equal(await tokenOwner.getAddress())
    })
  })

  describe("Upgrade Contract", async() => {
    let tokenOwner, Item, ItemV2, item, upgradedItem, newStats

    beforeEach(async() => {
      [deployer, tokenOwner, lendee] = await ethers.getSigners()
      Item = await ethers.getContractFactory("Item")
      ItemV2 = await ethers.getContractFactory("ItemV2")

      item = await upgrades.deployProxy(Item, [await tokenOwner.getAddress(), INIT_STATS], {
        initializer: "initialize",
      })

      await item.deployed()
      initStats = await item.stats(0)
      owner = await item.itemOwner()
    })

    it('should upgrade if contract owner upgrades', async() => {
      upgradedItem = await upgrades.upgradeProxy(item.address, ItemV2)

      newStats = await upgradedItem.stats(0)

      expect(newStats.name).to.be.equal('Durability')
      expect(Number(newStats.value)).to.be.equal(2)
    })
  })

  describe("Post contract upgrade", async() => {
    let deployer, tokenOwner, lendee, Item, ItemV2, item, initStats, owner, upgradedItem, newStats, lendeeAddress

    const reset = async() => {
      [deployer, tokenOwner, lendee] = await ethers.getSigners()
      Item = await ethers.getContractFactory("Item")
      ItemV2 = await ethers.getContractFactory("ItemV2")

      item = await upgrades.deployProxy(Item, [await tokenOwner.getAddress(), INIT_STATS], {
        initializer: "initialize",
      })

      await item.deployed()
      initStats = await item.stats(0)
      owner = await item.itemOwner()

      upgradedItem = await upgrades.upgradeProxy(item.address, ItemV2)
    }

    describe("Update Item stats", async() => {
      beforeEach(async() => {
        await reset();
      })
      it('should upgrade item stats', async() => {
        const is = await upgradedItem.connect(deployer).upgradeStat('Durability', 1)

        await is.wait()

        newStats = await upgradedItem.stats(0)

        expect(newStats.name).to.be.equal('Durability')
        expect(Number(newStats.value)).to.be.equal(3)
      })

      it('should downgrade item stats', async() => {
        const tx = await upgradedItem.connect(deployer).downgradeStat('Durability', 1)

        await tx.wait()

        newStats = await upgradedItem.stats(0)

        expect(newStats.name).to.be.equal('Durability')
        expect(Number(newStats.value)).to.be.equal(1)
      })

      it('should add new stat', async() => {
        const tx = await upgradedItem.connect(deployer).updateStat('Sharpness', 9)

        await tx.wait()

        newStats = await upgradedItem.stats(1)

        expect(newStats.name).to.be.equal('Sharpness')
        expect(Number(newStats.value)).to.be.equal(9)
      })

      it('should add not new stat as non contract owner', async() => {
        await expect(upgradedItem.connect(tokenOwner).updateStat('Sharpness', 9)).to.be.revertedWith('Ownable: caller is not the owner')

        newStats = await upgradedItem.stats(1)

        expect(newStats.name).to.be.equal('')
        expect(Number(newStats.value)).to.be.equal(0)
      })

      it('should not downgrade more than current value', async() => {
        await expect(upgradedItem.connect(deployer).downgradeStat('Durability', 5)).to.be.revertedWith('downgrading by too much')
      })

      it('should not allow non contract owner to upgrade', async() => {
        await expect(upgradedItem.connect(tokenOwner).upgradeStat('Durability', 1)).to.be.revertedWith('Ownable: caller is not the owner')
      })

      it('should not allow non contract owner to downgrade', async() => {
        await expect(upgradedItem.connect(tokenOwner).downgradeStat('Durability', 1)).to.be.revertedWith('Ownable: caller is not the owner')
      })
    })

    describe('lending out', async() => {
      beforeEach(async() => {
        await reset();
      })

      it('should lend if called by item owner', async() => {
        await upgradedItem.connect(tokenOwner).lendTo(lendee.getAddress())
        lendeeAddress = await upgradedItem.lentTo()  

        expect(await lendee.getAddress()).to.be.equal(lendeeAddress)
      })

      it('should rescind if called by item owner', async() => {
        await upgradedItem.connect(tokenOwner).rescindLend()
        lendeeAddress = await upgradedItem.lentTo()  

        expect(lendeeAddress).to.be.equal(zeroAddress)
      })

      it('should not lend if called by non item owner', async() => {
        await expect(upgradedItem.lendTo(lendee.getAddress())).to.be.revertedWith('only the item owner can do that')

        lendeeAddress = await upgradedItem.lentTo()
        expect(lendeeAddress).to.be.equal(zeroAddress)
      })

      it('should not resciend if called by non item owner', async() => {
        await expect(upgradedItem.rescindLend()).to.be.revertedWith('only the item owner can do that')
      })
    })
  })
});
